/*Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.*/

function createNewUser() {
  const newUser = {
    firstName: prompt("Введіть ім'я:"),
    lastName: prompt("Введіть прізвище:"),
    birthday: "",

    getLogin() {
      return (
        this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
      );
    },

    getAge() {
      const today = new Date();
      const [day, month, year] = this.birthday.split(".").map(Number);
      const birthDate = new Date(year, month - 1, day); // Місяці у JavaScript починаються з 0 (січень - 0, лютий - 1, і т.д.)

      // Перевірка, чи введена дата коректно
      if (
        isNaN(birthDate) ||
        birthDate.getFullYear() !== year ||
        birthDate.getMonth() + 1 !== month ||
        birthDate.getDate() !== day
      ) {
        alert("Некоректний формат дати!");
        this.birthday = prompt("Введіть дату народження у форматі dd.mm.yyyy:");
        return this.getAge(); // Рекурсивний виклик, якщо дата некоректна
      }

      let age = today.getFullYear() - birthDate.getFullYear();
      const monthDifference = today.getMonth() - birthDate.getMonth();

      if (
        monthDifference < 0 ||
        (monthDifference === 0 && today.getDate() < birthDate.getDate())
      ) {
        age--;
      }

      return age;
    },

    getPassword() {
      return (
        this.firstName.charAt(0).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6, 10)
      );
    },
  };

  newUser.birthday = prompt("Введіть дату народження у форматі dd.mm.yyyy:");

  return newUser;
}

const user1 = createNewUser();
console.log(user1);
console.log(user1.getLogin());
console.log("Age: ", user1.getAge());
console.log("Password: ", user1.getPassword());

//Домашнє завдання номер 5 (створена мною функція createNewUser())

// const user1 = createNewUser();
// console.log(user1);
// console.log(user1.getLogin());
// console.log("Age: ", user1.getAge());
// console.log("Password: ", user1.getPassword());

// function createNewUser() {
//   const newUser = {
//     firstName: prompt("Введіть ім'я:"),
//     lastName: prompt("Введіть прізвище:"),
//     getLogin() {
//       return (
//         this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
//       );
//     },
//   };

//   return newUser;
// }

// const user1 = createNewUser();
// console.log(user1);
// console.log(user1.getLogin());
